//! This is the build script of the EDCI Credential Visualizer. It will be run
//! by Cargo before the compilation of the main program.
//!
//! See the build.rs documentation for more info:
//! <https://doc.rust-lang.org/cargo/reference/build-scripts.html>

/// This build script sets the environment variables required by the EDCI
/// Credential Visualizer by printing to its stdout.

fn main() {
	println!("cargo:rustc-env=EDCI_OIDC_ADDRESS_ROOT=http://kubernetes.docker.internal:9000/auth/realms/edci");
	println!("cargo:rustc-env=EDCI_OIDC_LOGOUT_SUFFIX=/protocol/openid-connect/logout");
	println!("cargo:rustc-env=EDCI_OIDC_AUTH_REQUEST_SUFFIX=/protocol/openid-connect/auth");
	println!("cargo:rustc-env=EDCI_OIDC_TOKEN_SUFFIX=/protocol/openid-connect/token");
	println!("cargo:rustc-env=EDCI_OIDC_INTROSPECTION_SUFFIX=/protocol/openid-connect/token/introspect");
	println!("cargo:rustc-env=EDCI_OIDC_USER_INFO_SUFFIX=/protocol/openid-connect/token/userinfo");
	println!("cargo:rustc-env=EDCI_WALLET_ADDRESS_ROOT=http://localhost:8080/europass2/edci-wallet");
	println!("cargo:rustc-env=EDCI_WALLET_WALLETS_SUFFIX=/api/v1/wallets")
}
