# EDCI Credential Visualizer

This [Yew] application is used to visualize Europass-compliant credentials. It
communicates with an EDCI Wallet to get access to credentials and then
visualizes them.

## Compilation

Set the required environment variables (authentication server and EDCI Wallet
addresses) in the `build.rs` build script. Install [Rust] with [rustup]. Then
install the WebAssembly target with

	rustup target add wasm32-unknown-unknown

after which the project can be compiled with

	cargo build

You might also wish to install the Trunk WebAssembly application bundler with

	cargo install trunk

after which the application may be started with

	trunk serve --port 3000

and viewer by navigating to `localhost:3000` in a web browser of your choice
(which has to support WebAssembly, of course).


<!-- Hyperlinks -->

[Yew]: https://yew.rs/
[Rust]: https://www.rust-lang.org/
[rustup]: https://rustup.rs/
