//! A module that contains smaller components, that do not constitute entire
//! pages, such as the side bar and its items.

/// A sidebar component.

#[derive(PartialEq)]
pub struct Sidebar {
	pub title: &'static str,
}

impl yew::Component for Sidebar {

	type Message = ();
	type Properties = SidebarProps;

	fn create (_ctx: &yew::Context<Self>) -> Self {
		Self {
			title: "Sidebar"
		}
	}

	fn update (&mut self, _ctx: &yew::Context<Self>, _msg: Self::Message) -> bool {
		true
	}

	fn view (&self, _ctx: &yew::Context<Self>) -> yew::Html {

		yew::html! {
			<div>
				<h2>
					{ self.title }
				</h2>
				<div>
					{ for _ctx.props().children.clone() }
				</div>
			</div>
		}
	}
}

/// A helper type for storing the children of the Sidebar. The `children` field
/// is required by all Yew components that contain children.

#[derive(yew::Properties, PartialEq)]
pub struct SidebarProps {
	#[prop_or_default]
	pub children: yew::ChildrenWithProps<SidebarItem>,
}

/// These form the navigation items in the sidebar.

pub struct SidebarItem;

use crate::app::AppStateMsg;

impl yew::Component for SidebarItem {

	type Message = AppStateMsg;
	type Properties = SidebarItemProps;

	fn create (_ctx: &yew::Context<Self>) -> Self {
		Self
	}

	fn update (&mut self, _ctx: &yew::Context<Self>, _msg: Self::Message) -> bool {
		true
	}

	fn view (&self, _ctx: &yew::Context<Self>) -> yew::Html {

		let title = _ctx.props().title.clone();
		let to = _ctx.props().route.clone();
		let cb = _ctx.props().onclick.clone();

		yew::html! {
			<div onclick={cb}>
				<yew_router::prelude::Link<crate::routes::Route> to={ to } >
					{ title }
				</yew_router::prelude::Link<crate::routes::Route>>
			</div>
		}
	}
}

/// The properties of SidebarItems

#[derive(yew::Properties, PartialEq)]
pub struct SidebarItemProps {

	/// The title displayed by the sidebar item.

	pub title: String,

	/// The route that the sidebar item leads to.

	pub route: crate::routes::Route,

	/// The function that is called when the sidebar item is clicked.

	pub onclick: yew::Callback<yew::MouseEvent>
}
