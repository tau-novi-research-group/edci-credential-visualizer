//! The root module of the application. Contains the App-type.

/// The root components of the entire application.

pub struct App {
	title: &'static str,
	state: AppState,
}

/// A type for holding any global state of the application. This gets passed
/// down to child components via their contexts.

#[derive(Clone, PartialEq)]
pub struct AppState {

	/// Describes whether a user has logged in or not.

	pub logged_in: bool,

	/// Determines whether sidebar is opened or not, together with `logged_in`.

	pub sidebar_active: bool
}

/// Possible actions that could be taken to change app state.

#[derive(Debug)]
pub enum AppStateMsg {

	/// A user is navigating to the home page.

	Home,

	/// A user has clicked to log out button.

	Logout,

	/// A user has clicked the login button.

	Login,

	/// Intentionally do nothing.

	Noop,

	/// Unknown action. Do nothing.

	Unknown
}

impl yew::Component for App {

	type Message = AppStateMsg;
	type Properties = AppProps;

	fn create(_ctx: &yew::Context<Self>) -> Self {

		log::info!("Starting app with configuration {:#?}", &_ctx.props().config);

		Self {
			title: crate::constants::APP_NAME,
			state: AppState {
				logged_in: false,
				sidebar_active: true
			},
		}
	}

	fn update(&mut self, _ctx: &yew::Context<Self>, msg: Self::Message) -> bool {

		match msg {
			Self::Message::Login => {
				self.state.logged_in = true;
			},
			Self::Message::Logout => {
				self.state.logged_in = false;
			},
			_ => {}
		};
		true
	}

	fn view(&self, _ctx: &yew::Context<Self>) -> yew::Html {

		// Define callback passed to children

		let noop = _ctx.link().callback(|_: yew::MouseEvent| Self::Message::Noop);
		let logout = _ctx.link().callback(|_: yew::MouseEvent| Self::Message::Logout);

		// Passed to the Login page. The login form there has an onsubmit
		// event listener, which requires a FocusEvent type as a parameter.

		let login = _ctx.link().callback( move |_: yew::FocusEvent| {

			// TODO: this closure needs to send a login request to the
			// authentication provider.

			Self::Message::Login
		});

		// Define the switch used by the crate::routes::Router to change
		// pages. This is a single-use closure, as yew_router::Switch::render
		// (used below) expects a function with only one argument, and hence
		// capturing the environment is needed.

		use yew_router::prelude::Redirect;
		use crate::routes::Route;

		let state = self.state.clone();

		let switch = move |route : &crate::routes::Route| {

			let logged_in = state.logged_in;

			match (route, logged_in) {
				(crate::routes::Route::Home, true) => yew::html! {
					<crate::pages::Home />
				},
				(crate::routes::Route::Home, false) => yew::html! {
					<Redirect<Route> to={Route::Login} />
				},
				(crate::routes::Route::Login, true) => yew::html! {
					<Redirect<Route> to={Route::Home} />
				},
				(crate::routes::Route::Login, false) => yew::html! {
					<crate::pages::Login login_method={ login.clone() }/>
				},
				(crate::routes::Route::Logout, _) => yew::html! {
					<crate::pages::Login login_method={ login.clone() }/>
				},
				(crate::routes::Route::NotFound, _) => yew::html! {
					<crate::pages::Page404 />
				},
			}
		};

		// Define the structure of the Application view.

		yew::html! {
			<>
				<h1>{ self.title }</h1>

				// ContextProvider automatically takes care of passing
				// AppState to all its children recursively.

				<yew::ContextProvider<AppState> context={ self.state.clone() }>

					// BrowserRouter automatically changes the page depending
					// on the address being viewed.

					<yew_router::BrowserRouter>

						// Show sidebar if logged in.

						if self.state.logged_in && self.state.sidebar_active {
							<crate::components::Sidebar>
								<crate::components::SidebarItem
									title={ crate::constants::HOME_NAME }
									route={ crate::routes::Route::Home }
									onclick={ noop.clone() }
								/>
								<crate::components::SidebarItem
									title={ crate::constants::LOGOUT_NAME }
									route={ crate::routes::Route::Login }
									onclick={ logout.clone() }
								/>
							</crate::components::Sidebar>
						}

						<main>
							<yew_router::Switch<crate::routes::Route>
								render = {
									yew_router::Switch::render(switch)
								}
							/>
						</main>

					</yew_router::BrowserRouter>
				</yew::ContextProvider<AppState>>
			</>
		}
	}
}

/// Properties of the main App.

#[derive(yew::Properties, PartialEq)]
pub struct AppProps {

	/// The configuration of the application, read in from environment
	/// variables.

	pub config: crate::config::Config,
}
