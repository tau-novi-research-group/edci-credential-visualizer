//! The module that contains the different page components.

/// The component for the login page.

#[yew::function_component(Login)]
pub fn login(props: &LoginProps) -> yew::Html {

	yew::html! {
		<div>
			<h2>{ crate::constants::LOGIN_NAME }</h2>

			<form onsubmit={ props.login_method.clone() }>
				<label for="username"> { "Username:" } </label>
				<input type="text" id="username" name="username" />
				<label for="password"> { "Password:" } </label>
				<input type="text" id="password" name="password" />
				<input type="submit" value={ crate::constants::LOGIN_NAME } />
			</form>

			<p>
				{
					"This page is used to sign in to the same OIDC provider
					that the EDCI applications are connected to. To this end,
					the application needs to be aware of the following
					environment variables: "
				}
			</p>

			<ol>
				<li><code>{ "EDCI_OIDC_ADDRESS_ROOT" }</code></li>
				<li><code> { "EDCI_WALLET_ADDRESS_ROOT" } </code></li>
				<li><code> { "EDCI_OIDC_LOGOUT_SUFFIX" } </code></li>
				<li><code> { "EDCI_OIDC_AUTH_REQUEST_SUFFIX" } </code></li>
				<li><code> { "EDCI_OIDC_INTROSPECTION_SUFFIX" } </code></li>
			</ol>

			<p>
				{
					"Yes, there are quite a few, but the names of these
					variables should be pretty self-explanatory. The suffixes
					are appended to the related root address as-is, meaning
					you must take care not to introduce too many or too few
					path separators '/'."
				}
			</p>
		</div>
	}
}

/// Properties of the Login page.

#[derive(yew::Properties, PartialEq)]
pub struct LoginProps {

	/// The event handler which will be called when the login button is
	/// pressed.

	pub login_method: yew::Callback<yew::FocusEvent>
}


/// The home page component.

pub struct Home {
	title: &'static str,
}

impl yew::Component for Home {

	type Message = ();
	type Properties = ();

	fn create (_ctx: &yew::Context<Self>) -> Self {
		Self {
			title: crate::constants::HOME_NAME
		}
	}

	fn update (&mut self, _ctx: &yew::Context<Self>, _msg: Self::Message) -> bool {
		true
	}

	fn view (&self, _ctx: &yew::Context<Self>) -> yew::Html {
		yew::html! {
			<div>
				<h2>
					{ self.title }
				</h2>
			</div>
		}
	}
}

/// The 404 page component.

pub struct Page404 {
	title: &'static str,
}

impl yew::Component for Page404 {

	type Message = ();
	type Properties = ();

	fn create (_ctx: &yew::Context<Self>) -> Self {
		Self {
			title: crate::constants::NOT_FOUND_NAME
		}
	}

	fn update (&mut self, _ctx: &yew::Context<Self>, _msg: Self::Message) -> bool {
		true
	}

	fn view (&self, _ctx: &yew::Context<Self>) -> yew::Html {
		yew::html! {
			<div>
				<h2>
					{ self.title }
				</h2>
			</div>
		}
	}
}
