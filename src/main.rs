//! # EDCI Credential Visualizer
//!
//! A [Yew] application that visualizes [EDCI]-compliant digital credentials.
//!
//! [Yew]: https://yew.rs/
//! [EDCI]: https://ec.europa.eu/futurium/en/system/files/ged/edci_presentation.pdf

use yew;

mod app;
mod config;
mod constants;
mod pages;
mod components;
mod routes;

/// The main entrypoint of the program.

fn main() -> Result<(), MainError> {

	// Start a gloval logger object

	wasm_logger::init(wasm_logger::Config::default());

	// Create an App properties object…

	use config::ConfigCreationResult;

	let config = config::Config::try_new();

	let props = app::AppProps {
		config: match config {
			ConfigCreationResult::Ok(c) => c,
			_ => {
				log::error!("{:?}", config.to_string());
				return Err(MainError::ConfigCreationFailure)
			}
		}
	};

	// … and then pass it onto the App.

	yew::start_app_with_props::<app::App>(props);

	Ok(())
}

/// The ways in which the main function can fail.

#[derive(Debug)]
enum MainError {

	/// Could not create configuration object to be passed into the
	/// application properties.

	ConfigCreationFailure,
}
