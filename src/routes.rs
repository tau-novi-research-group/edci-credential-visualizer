//! A module that contains the router of the application.

use yew_router::prelude::*;
use yew::prelude::*;

/// The route enum. Defines the set of addresses that the app recognises, and
/// assigns a 404 page to the unknown ones.

#[derive(Clone, Routable, PartialEq)]
pub enum Route {

	/// The home page.

	#[at("/")]
	Home,

	/// The login page

	#[at("/login")]
	Login,

	/// The logout page

	#[at("/logout")]
	Logout,

	/// Page for unrecognised addresses.

	#[not_found]
	#[at("/404")]
	NotFound,
}

impl std::fmt::Display for Route {

	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {

		let s = match self {
			Route::Home => crate::constants::HOME_NAME,
			Route::Login => crate::constants::LOGIN_NAME,
			Route::Logout => crate::constants::LOGOUT_NAME,
			Route::NotFound => crate::constants::NOT_FOUND_NAME,
		};

		write!(f, "{}", s)
	}
}

/// Route functionality.

impl Route {

	/// Returns the path of each Route variant.

	fn path (&self) -> &str {

		match self {
			Self::Home => "/",
			Self::Login => "/login",
			Self::Logout => "/logout",
			Self::NotFound => "/404",
		}
	}
}
