//! A module that contains an implementation for an application configuration object.

/// The configuration object of this application. Holds information such as
/// the OIDC provider and EDCI Wallet addresses, and is constructed from
/// environment variables at program start.

#[derive(Debug, PartialEq)]
pub struct Config {

	/// The address of an OpenID Connect provider (an OAuth 2 -based
	/// authentication server). The EDCI folks recommend [Keycloak] as a
	/// viable option, at least in test environments.
	///
	/// [Keycloak]: https://hub.docker.com/r/jboss/keycloak/

	oidc_address_root: String,

	/// The full address to the authentication provider logout endpoint. When
	/// a user logs out, infromation of this should be sent here, so that any
	/// tokens are revoked.

	oidc_logout_address: String,

	/// The full address to the authentication provider authentication request
	/// address.

	oidc_auth_request_address: String,

	/// The full address to the authentication provider token introspection
	/// endpoint.

	oidc_introspection_address: String,

	/// This is the root address of the EDCI Wallet.

	edci_wallet_address_root: String,

	/// The EDCI Wallet endpoint where the wallets of users can be accessed.

	edci_wallet_wallets_address: String,
}

/// Config method implementations.

impl Config {

	/// The name of the required environment variable holding the OIDC
	/// provider address. In Keycloak this is usually
	///
	///		https://<host>:<port>/auth/realms/<realm> .
	///
	/// Here the `<realm>` should probably be ´edci´, but it corresponds to the
	/// one set in you OIDC provider.

	pub const OIDC_ADDRESS_ROOT: &'static str = "EDCI_OIDC_ADDRESS_ROOT";

	/// The name of the environment variable holding the root of the EDCI
	/// Wallet address.

	pub const EDCI_WALLET_ADDRESS_ROOT: &'static str = "EDCI_WALLET_ADDRESS_ROOT";

	/// The suffix attached to the OIDC address root, which determines the
	/// logout address that should be informed of the user having logged out.
	/// Needs to be given as an environment variable.

	pub const OIDC_LOGOUT_SUFFIX: &'static str = "EDCI_OIDC_LOGOUT_SUFFIX";

	/// The address that any OIDC authentication requests should be sent to.
	/// This is attached to the root of the OIDC provider address. Needs to be
	/// given as an environment variable.

	pub const OIDC_AUTH_REQUEST_SUFFIX: &'static str = "EDCI_OIDC_AUTH_REQUEST_SUFFIX";

	/// The OIDC provider endpoint that inspects authentication tokens the
	/// provider has issued and makes sure they are still valid.

	pub const OIDC_INTROSPECTION_SUFFIX: &'static str = "EDCI_OIDC_INTROSPECTION_SUFFIX";

	/// A constructor for the `Config` object. Reads the required environment
	/// variables, and if all of them have been set, returns a Config object
	/// wrapped in a ConfigCreationResult.

	pub fn try_new () -> ConfigCreationResult {

		// Fetch environment variables

		let oidc_address_root = match std::option_env!("EDCI_OIDC_ADDRESS_ROOT") {
			Some(addr) => addr,
			None => return ConfigCreationResult::OIDCAddressRootMissing
		};

		let oidc_logout_address_suffix = match std::option_env!("EDCI_OIDC_LOGOUT_SUFFIX") {
			Some(addr) => addr,
			None => return ConfigCreationResult::OIDCLogoutSuffixMissing
		};

		let oidc_auth_request_address_suffix = match std::option_env!("EDCI_OIDC_AUTH_REQUEST_SUFFIX") {
			Some(addr) => addr,
			None => return ConfigCreationResult::OIDCLogoutSuffixMissing
		};

		let oidc_introspection_address_suffix = match std::option_env!("EDCI_OIDC_INTROSPECTION_SUFFIX") {
			Some(addr) => addr,
			None => return ConfigCreationResult::OIDCIntrospectionSuffixMissing
		};

		let edci_wallet_address_root = match std::option_env!("EDCI_WALLET_ADDRESS_ROOT") {
			Some(addr) => addr,
			None => return ConfigCreationResult::EDCIWalletAddressRootMissing
		};

		let edci_wallet_wallets_suffix = match std::option_env!("EDCI_WALLET_WALLETS_SUFFIX") {
			Some(addr) => addr,
			None => return ConfigCreationResult::EDCIWalletAddressRootMissing
		};

		// Create and return Config object.

		ConfigCreationResult::Ok( Self {
			oidc_address_root: String::from(oidc_address_root),
			oidc_logout_address: String::from(oidc_address_root) + oidc_logout_address_suffix,
			oidc_introspection_address: String::from(oidc_address_root) + oidc_introspection_address_suffix,
			oidc_auth_request_address: String::from(oidc_address_root) + oidc_auth_request_address_suffix,
			edci_wallet_address_root: String::from(edci_wallet_address_root),
			edci_wallet_wallets_address: String::from(edci_wallet_address_root) + edci_wallet_wallets_suffix,
		})
	}
}

/// An enumeration of the results of the `try_new` method of the config
/// object.

pub enum ConfigCreationResult {

	/// Config creation successful.

	Ok(Config),

	/// An OIDC root address was not found in the environment.

	OIDCAddressRootMissing,

	/// An EDCI Wallet address was not found in the environment.

	EDCIWalletAddressRootMissing,

	/// The logout address suffix that is to be attached to the OIDC address
	/// root URL was missing.

	OIDCLogoutSuffixMissing,

	/// The authentication request address suffix that is to be attached to
	/// the OIDC address root URL was missing.

	OIDCAuthRequestSuffixMissing,

	/// The token introspection address suffix that is to be attached to the
	/// OIDC address root URL was missing.

	OIDCIntrospectionSuffixMissing,
}

/// Display implementation for

impl std::fmt::Display for ConfigCreationResult {

	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {

		let m = "Missing";

		let s = match self {
			ConfigCreationResult::Ok(_) => String::from("Ok"),
			ConfigCreationResult::OIDCAddressRootMissing => format!("{} {}", &m, Config::OIDC_ADDRESS_ROOT),
			ConfigCreationResult::EDCIWalletAddressRootMissing => format!("{} {}", &m, Config::EDCI_WALLET_ADDRESS_ROOT),
			ConfigCreationResult::OIDCLogoutSuffixMissing => format!("{} {}", &m, Config::OIDC_LOGOUT_SUFFIX),
			ConfigCreationResult::OIDCAuthRequestSuffixMissing => format!("{} {}", &m, Config::OIDC_AUTH_REQUEST_SUFFIX),
			ConfigCreationResult::OIDCIntrospectionSuffixMissing => format!("{} {}", &m, Config::OIDC_INTROSPECTION_SUFFIX),
		};

		write!(f, "{}", s)
	}
}
