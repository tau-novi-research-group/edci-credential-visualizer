//! A module for common constants.

/// The application name.

pub const APP_NAME: &str = "EDCI Credential Visualizer";

/// The home page name.

pub const HOME_NAME: &str = "Home";

/// The login page name.

pub const LOGIN_NAME: &str = "Login";

/// The logout page name.

pub const LOGOUT_NAME: &str = "Logout";

/// The 404 page name.

pub const NOT_FOUND_NAME: &str = "404 Not found";
